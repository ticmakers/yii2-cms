<?php

namespace ticmakers\cms;

/**
 * cms module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'ticmakers\cms\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        // inicializa el módulo con la configuración cargada desde config.php
        \Yii::configure($this, require __DIR__ . '/config/main.php');
    }
}
