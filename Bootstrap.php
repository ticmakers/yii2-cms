<?php

namespace ticmakers\cms;

use yii\base\Application;
use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package ticmakers\yii2-notifications
 * @author Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if (!$app->hasModule('cms')) {
            $app->setModule('cms', 'ticmakers\cms\Module');
        }
    }
}
